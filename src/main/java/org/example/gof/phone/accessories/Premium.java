package org.example.gof.phone.accessories;

public class Premium extends Accessory {

    private static final double PRICE = 10_000;
    private static final String FEATURE = "Premium quality";

    public Premium() {
        super(PRICE, FEATURE);
    }
}
