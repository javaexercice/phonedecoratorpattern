package org.example.gof.phone.accessories;

public class Accessory {

    private final double PRICE;
    private final String FEATURE;

    public Accessory(double PRICE, String FEATURE) {
        this.PRICE = PRICE;
        this.FEATURE = FEATURE;
    }

    public double getPrice() {
        return PRICE;
    }

    public String getFeature() {
        return FEATURE;
    }
}
