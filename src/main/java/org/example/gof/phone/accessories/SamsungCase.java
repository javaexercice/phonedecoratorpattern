package org.example.gof.phone.accessories;

public class SamsungCase extends Accessory {

    private static final double PRICE = 50;
    private static final String FEATURE = "Samsung case";

    public SamsungCase() {
        super(PRICE, FEATURE);
    }

    @Override
    public double getPrice() {
        return super.getPrice();
    }

    @Override
    public String getFeature() {
        return super.getFeature();
    }
}
