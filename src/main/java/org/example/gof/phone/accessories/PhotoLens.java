package org.example.gof.phone.accessories;

public class PhotoLens extends Accessory {

    private static final double PRICE = 400;
    private static final String FEATURE = "ZOOM * 100";

    public PhotoLens() {
        super(PRICE, FEATURE);
    }
}
