package org.example.gof.phone.accessories;

public class Case extends Accessory {

    private static final double PRICE = 5;
    private static final String FEATURE = "Basic case";

    public Case() {
        super(PRICE, FEATURE);
    }
}
