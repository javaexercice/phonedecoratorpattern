package org.example.gof.phone.decorator;

import org.example.gof.phone.Phone;

public class YuanMoneyDecorator extends PhoneDecorator{

    public YuanMoneyDecorator(Phone phone) {
        super(phone);
    }

    @Override
    public double getPrice() {
        return super.getPrice() * 7.06;
    }
}
