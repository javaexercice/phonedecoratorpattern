package org.example.gof.phone.decorator;

import org.example.gof.phone.Phone;

public class PhoneDecorator implements Phone {

    private final Phone phone;

    public PhoneDecorator(Phone phone) {
        this.phone = phone;
    }

    @Override
    public String getFeature() {
        return phone.getFeature();
    }

    @Override
    public double getPrice() {
        return phone.getPrice();
    }
}
