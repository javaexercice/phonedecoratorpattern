package org.example.gof.phone.decorator;

import org.example.gof.phone.accessories.Accessory;
import org.example.gof.phone.accessories.Premium;
import org.example.gof.phone.Phone;

public class PremiumDecorator extends PhoneDecorator {

    private static final Accessory ACCESSORY = new Premium();

    public PremiumDecorator(Phone phone) {
        super(phone);
    }

    @Override
    public String getFeature() {
        return super.getFeature() + ACCESSORY.getFeature();
}

    @Override
    public double getPrice() {
        System.out.println("hey" +super.getPrice());
        return super.getPrice() + ACCESSORY.getPrice();
    }
}
