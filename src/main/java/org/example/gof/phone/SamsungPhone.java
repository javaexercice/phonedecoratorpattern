package org.example.gof.phone;

import org.example.gof.phone.accessories.Accessory;
import org.example.gof.phone.accessories.PhotoLens;
import org.example.gof.phone.accessories.SamsungCase;

import java.util.List;

public class SamsungPhone extends BasicPhone {

    private static final List<Accessory> ACCESSORIES = List.of(new PhotoLens(), new SamsungCase());
    private static final double PRICE = 1000;

    public SamsungPhone() {
        super(ACCESSORIES, PRICE);
    }

}
