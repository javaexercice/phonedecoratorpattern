package org.example.gof.phone;

public interface Phone {

    String getFeature();

    double getPrice();
}
