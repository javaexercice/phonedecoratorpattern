package org.example.gof.phone;

import org.example.gof.phone.accessories.Accessory;
import org.example.gof.phone.accessories.Case;
import org.example.gof.phone.accessories.PhotoLens;

import java.util.List;
import java.util.stream.Collectors;

public class BasicPhone implements Phone {

    private final List<Accessory> accessories;
    private final double price;

    public BasicPhone() {
        this.price = 400;
        this.accessories = List.of(new Case(), new PhotoLens());
    }

    public BasicPhone(List<Accessory> accessories, double price) {
        this.accessories = accessories;
        this.price = price;
    }

    @Override
    public String getFeature() {
        return accessories.stream()
                       .map(Accessory::getFeature)
                       .collect(Collectors.joining(","));
    }

    @Override
    public double getPrice() {
        return price + accessories.stream()
                               .mapToDouble(Accessory::getPrice)
                               .sum();
    }
}
