package org.example.gof;

import org.example.gof.phone.BasicPhone;
import org.example.gof.phone.Phone;
import org.example.gof.phone.SamsungPhone;
import org.example.gof.phone.decorator.PhoneDecorator;
import org.example.gof.phone.decorator.PremiumDecorator;
import org.example.gof.phone.decorator.YuanMoneyDecorator;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {

        BasicPhone basicPhone = new SamsungPhone();
        System.out.println(basicPhone.getFeature());
        System.out.println(basicPhone.getPrice());

        SamsungPhone samsungPhone = new SamsungPhone();
        System.out.println(samsungPhone.getFeature());
        System.out.println(samsungPhone.getPrice());


        Phone premiumDecorator = new PhoneDecorator(
                new YuanMoneyDecorator(
                        new PremiumDecorator(
                                new SamsungPhone())));

        System.out.println(premiumDecorator.getFeature());
        System.out.println(premiumDecorator.getPrice());
    }
}
